//Ground Stuff
ground_friction = .65;
moonwalk_accel = 1.4;

walk_speed = 3.5;
walk_accel = 0.3;
walk_turn_time = 6;

initial_dash_time = 11;
initial_dash_speed = 7;

dash_speed = 6;
dash_turn_time = 20;
dash_turn_accel = 0.5;
dash_stop_time = 6;
dash_stop_percent = .4; //the value to multiply your hsp by when going into idle from dash or dashstop

roll_forward_max = 10.5; //roll speed
roll_backward_max = 10.5;


//Jump Stuff
jump_start_time = 4; //startup frames (everybody gets a 4)
jump_speed = 16; //velocity
short_hop_speed = 9; //velocity
djump_speed = 14; //velocity

leave_ground_max = 4; //the maximum hsp you can have when you go from grounded to aerial without jumping
max_jump_hsp = 4; //the maximum hsp you can have when jumping from the ground


//Air Mobility
jump_change = 3; //maximum hsp when double jumping. If already going faster, it will not slow you down
air_max_speed = 4; //the maximum hsp you can accelerate to when in a normal aerial state
air_accel = .3; // horizontal air acceleration
air_friction = .035; //Natural (survival) deceleration.
prat_fall_accel = 1.2; //multiplier of air_accel while in pratfall

max_djumps = 1;

walljump_hsp = 7;
walljump_vsp = 11;
walljump_time = 32;

max_fall = 12; //maximum fall speed without fastfalling
fast_fall = 16; //fast fall speed
gravity_speed = .7;
hitstun_grav = .55;
knockback_adj = 1.1; //the multiplier to KB dealt to you. 1 = default, >1 = lighter, <1 = heavier


//Landing
land_time = 4; //normal landing frames
prat_land_time = 3;
wave_land_time = 8;
wave_land_adj = 1.25; //the multiplier to your initial hsp when wavelanding. Usually greater than 1
wave_friction = .07; //grounded deceleration when wavelanding




//Sprite stuff
char_height = 56;
hurtbox_spr = asset_get("ex_guy_hurt_box");
crouchbox_spr = asset_get("ex_guy_crouch_box");
air_hurtbox_spr = -1;
hitstun_hurtbox_spr = -1;


//Animation Stuff
idle_anim_speed = .1;
crouch_anim_speed = .1;
walk_anim_speed = .125;
dash_anim_speed = .2;
pratfall_anim_speed = .25;
double_jump_time = 32; // # of frames to play the djump animation (make the transition to fall seamless)

//crouch animation frames
crouch_startup_frames = 1;
crouch_active_frames = 1;
crouch_recovery_frames = 1;

//parry animation frames
dodge_startup_frames = 1;
dodge_active_frames = 1;
dodge_recovery_frames = 3;

//tech animation frames
tech_active_frames = 3;
tech_recovery_frames = 1;

//tech roll animation frames
techroll_startup_frames = 2
techroll_active_frames = 2;
techroll_recovery_frames = 2;
techroll_speed = 10;

//airdodge animation frames
air_dodge_startup_frames = 1;
air_dodge_active_frames = 2;
air_dodge_recovery_frames = 3;
air_dodge_speed = 7.5;

//roll animation frames
roll_forward_startup_frames = 2;
roll_forward_active_frames = 4;
roll_forward_recovery_frames = 2;
roll_back_startup_frames = 2;
roll_back_active_frames = 4;
roll_back_recovery_frames = 2;


//Sounds
land_sound = asset_get("sfx_land_med");
landing_lag_sound = asset_get("sfx_land");
waveland_sound = asset_get("sfx_waveland_zet");
jump_sound = asset_get("sfx_jumpground");
djump_sound = asset_get("sfx_jumpair");
air_dodge_sound = asset_get("sfx_quick_dodge");


//Last but not least: visual offsets for when you're in Ranno's bubble
bubble_x = 0;
bubble_y = 8;
