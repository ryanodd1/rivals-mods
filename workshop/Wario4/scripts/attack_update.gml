if (attack == AT_DATTACK || attack == AT_FAIR){
    can_fast_fall = false; //resets on its own
    if (window == 1 && has_hit_player) {
        window = 3;
        window_timer = 0;
    }
    if ( window == 3){
        if (window_timer == 1){
            hsp = spr_dir * -3;
            vsp = -4;
            air_accel = 0;
        }
        
    }
    if (jump_pressed && window == 1){
        if (!free){
            vsp = -9;
        }    
        //doublejump if in the air and not moving up
        else if (free && djumps == 0 && vsp >= 0){
            vsp = -9;
            djumps = 1;
        }
    }
}

if (attack == AT_DAIR){
    //weird fix for w1 mixing with fastfall
    if (window == 1 && window_timer == 1){
        can_fast_fall = false; //resets on its own
    }

    //w2 has super armor
    if (window == 2 && window_timer == 1){
        super_armor = true;
    }

    //end w2 loop when ground is hit
    if (window == 2 && !free){      
        window = 3;
        window_timer = 0;
    }
    //transfer to hitlag on hit
    if (window == 2 && has_hit_player) {
        window = 4;
        window_timer = 0;
    }
    //hitlag
    if (window == 4){
        if (window_timer == 1){
            vsp = -4;
            air_accel = 0.3;
            can_fast_fall = false; //resets on its own
        }
    }
}

if (attack == AT_NSPECIAL){
    if (window == 1 || window == 2){
        if (up_down && !c_nspecial_aiming_up){
            set_attack_value(AT_NSPECIAL, AG_SPRITE, sprite_get("nspecialup"));
            set_attack_value(AT_NSPECIAL, AG_HURTBOX_SPRITE, sprite_get("nspecial_hurtup"));
            c_nspecial_aiming_up = true;
        }
        else if (!up_down && c_nspecial_aiming_up){
            set_attack_value(AT_NSPECIAL, AG_SPRITE, sprite_get("nspecial"));
            set_attack_value(AT_NSPECIAL, AG_HURTBOX_SPRITE, sprite_get("nspecial_hurt"));
            c_nspecial_aiming_up = false;
        }
    }

    if (!c_nspecial_charged && window == 2 && window_timer == 3){
        c_nspecial_charged = true;
    }

    if (window == 2 && !special_down){
        window = 3;
        window_timer = 0;
    }
}

if (attack == AT_FSPECIAL){
    if (window <= 13){
        if (!c_can_cancel_fspecial && !special_pressed){
            c_can_cancel_fspecial = true;
        }
        else if (c_can_cancel_fspecial && special_pressed){
            window = 14;
            window_timer = 0;
        }
    }
}

if (attack == AT_USPECIAL){
    //weird fix for w1 mixing with fastfall
    if (window == 1 && window_timer == 1){
        can_fast_fall = false; //resets on its own
    }

    //final frame of w1, apply force. I do it here because w2 loops and keeps reapplying the force
    if (window == 2 && window_timer == 1 && vsp >= -1){   
        vsp = -25;

        //both need to be reset
        air_max_speed = 2;
        air_accel = 0.3;
        gravity_speed = .7;
    }
    //end w2 loop when peak is reached
    if (window == 2 && vsp >= 0){      
        window = 3;
        window_timer = 0;
        c_can_uspecial = false;
        gravity_speed = .1;
    }
}

if (attack == AT_DSPECIAL){
    if (window == 1 && window_timer == 1){
        move_cooldown[AT_DSPECIAL] = 72;
    }
}


/////////////////////////////////////////////////////

// //B - Reversals
// if (attack == AT_NSPECIAL || attack == AT_FSPECIAL || attack == AT_DSPECIAL || attack == AT_USPECIAL){
//     trigger_b_reverse();
// }

// if (attack == AT_NSPECIAL){
//     if (window == 3){
//         if (special_pressed){
//             window = 1;
//             window_timer = 0;
//         }
//     }
// }

// if (attack == AT_FSPECIAL){
//     if (window == 2){
//         if (special_pressed){
//             window = 3;
//             window_timer = 0;
//             destroy_hitboxes();
//         }
//     }
//     can_fast_fall = false;
// }

// if (attack == AT_USPECIAL){
//     if (window == 1 && window_timer == 1){
//         times_through = 0;
//     }
//     if (window == 2){
//         if (window_timer == get_window_value(attack, 2, AG_WINDOW_LENGTH)){
//             if (times_through < 10){
//                 times_through++;
//                 window_timer = 0;
//             }
//         }
//         if (!joy_pad_idle){
//             hsp += lengthdir_x(1, joy_dir);
//             vsp += lengthdir_y(1, joy_dir);
//         } else {
//             hsp *= .6;
//             vsp *= .6;
//         }
//         var fly_dir = point_direction(0,0,hsp,vsp);
//         var fly_dist = point_distance(0,0,hsp,vsp);
//         var max_speed = 12;
//         if (fly_dist > max_speed){
//             hsp = lengthdir_x(max_speed, fly_dir);
//             vsp = lengthdir_y(max_speed, fly_dir);
//         }
//         if (special_pressed && times_through > 0){
//             window = 4;
//             window_timer = 0;
//         }
//         if (shield_pressed){
//             window = 3;
//             window_timer = 0;
//         }
//     }
//     if (window > 3 && window < 6 && window_timer == get_window_value(attack, window, AG_WINDOW_LENGTH)){
//         window++;
//         window_timer = 0;
//     }
// }

// if (attack == AT_DSPECIAL){
//     if (window == 2){
//         can_jump = true;
//     }
//     can_fast_fall = false;
//     can_move = false
// }
