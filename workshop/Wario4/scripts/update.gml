//Every frame.

//These mostly reset some value that was changed during an attack

//DATTACK & FAIR have air accel = 0 in window 2. This resets it
if  (air_accel != 0.6 
&& !((attack == AT_DATTACK || attack == AT_FAIR) && window == 3)
&& !(attack == AT_DAIR && window == 4)
&& !(attack == AT_USPECIAL && (window == 2 || window == 3))){
    air_accel = 0.6;
}

if (super_armor && !(attack == AT_DAIR && window == 2)){
    super_armor = false;
}

if (air_max_speed != 5 && !(attack == AT_USPECIAL && window == 2)){
    air_max_speed = 5;
}

if (gravity_speed != 0.4
&& !(attack == AT_USPECIAL && window == 2)
&& !(attack == AT_USPECIAL && window == 3)){
    gravity_speed = 0.4;
}


//Resetting Custom Variables
if (c_nspecial_aiming_up
&& !(attack == AT_NSPECIAL && (window == 2 || window == 3))){
    set_attack_value(AT_NSPECIAL, AG_SPRITE, sprite_get("nspecial"));
    set_attack_value(AT_NSPECIAL, AG_HURTBOX_SPRITE, sprite_get("nspecial_hurt"));
    c_nspecial_aiming_up = false;
}

if (c_nspecial_charged
&& !(attack == AT_NSPECIAL && (window == 2 || window == 3))){
    c_nspecial_charged = false;
}

if (!c_can_uspecial){
    if (free || state == PS_HITSTUN){
        move_cooldown[AT_USPECIAL] = 2; // 1 is not enough
    }
    else{
        c_can_uspecial = true;
    }
}

if (c_can_cancel_fspecial && !(attack == AT_FSPECIAL && (window >= 1 && window <= 14))){
    c_can_cancel_fspecial = false;
}

