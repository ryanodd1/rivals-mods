set_attack_value(AT_DAIR, AG_CATEGORY, 1);
set_attack_value(AT_DAIR, AG_SPRITE, sprite_get("dair"));
set_attack_value(AT_DAIR, AG_HURTBOX_SPRITE, sprite_get("dair_hurt"));
set_attack_value(AT_DAIR, AG_NUM_WINDOWS, 3);
set_attack_value(AT_DAIR, AG_HAS_LANDING_LAG, 0); //for landing in window 1? may want to disable
//set_attack_value(AT_DAIR, AG_USES_CUSTOM_GRAVITY, 1);????



set_window_value(AT_DAIR, 1, AG_WINDOW_TYPE, 0);
set_window_value(AT_DAIR, 1, AG_WINDOW_LENGTH, 12);
set_window_value(AT_DAIR, 1, AG_WINDOW_ANIM_FRAMES, 1);


set_window_value(AT_DAIR, 1, AG_WINDOW_HAS_CUSTOM_FRICTION, 1);
set_window_value(AT_DAIR, 1, AG_WINDOW_CUSTOM_AIR_FRICTION, 1.2);

//set_window_value(AT_DAIR, 1, AG_WINDOW_HSPEED_TYPE, 1);
set_window_value(AT_DAIR, 1, AG_WINDOW_VSPEED_TYPE, 1);
//set_window_value(AT_DAIR, 1, AG_WINDOW_HSPEED, 0);
set_window_value(AT_DAIR, 1, AG_WINDOW_VSPEED, 0);



set_window_value(AT_DAIR, 2, AG_WINDOW_TYPE, 9);
set_window_value(AT_DAIR, 2, AG_WINDOW_LENGTH, 6);
set_window_value(AT_DAIR, 2, AG_WINDOW_ANIM_FRAMES, 6);
set_window_value(AT_DAIR, 2, AG_WINDOW_ANIM_FRAME_START, 2);

set_window_value(AT_DAIR, 2, AG_WINDOW_HSPEED_TYPE, 1);
set_window_value(AT_DAIR, 2, AG_WINDOW_VSPEED_TYPE, 1);
set_window_value(AT_DAIR, 2, AG_WINDOW_HSPEED, 0);
set_window_value(AT_DAIR, 2, AG_WINDOW_VSPEED, 12);





set_window_value(AT_DAIR, 3, AG_WINDOW_TYPE, 0);
set_window_value(AT_DAIR, 3, AG_WINDOW_LENGTH, 36);
set_window_value(AT_DAIR, 3, AG_WINDOW_ANIM_FRAMES, 12);
set_window_value(AT_DAIR, 3, AG_WINDOW_ANIM_FRAME_START, 8);

set_window_value(AT_DAIR, 3, AG_WINDOW_HSPEED_TYPE, 1);
set_window_value(AT_DAIR, 3, AG_WINDOW_VSPEED_TYPE, 1);
set_window_value(AT_DAIR, 3, AG_WINDOW_HSPEED, 0);
set_window_value(AT_DAIR, 3, AG_WINDOW_VSPEED, 0);

set_window_value(AT_DAIR, 3, AG_WINDOW_HAS_SFX, 1);
set_window_value(AT_DAIR, 3, AG_WINDOW_SFX, asset_get("sfx_orca_bite"));
set_window_value(AT_DAIR, 3, AG_WINDOW_SFX_FRAME, 1);

//hit lag window (not normally reached)
set_window_value(AT_DAIR, 4, AG_WINDOW_TYPE, 0);
set_window_value(AT_DAIR, 4, AG_WINDOW_LENGTH, 12);
set_window_value(AT_DAIR, 4, AG_WINDOW_ANIM_FRAMES, 6);
set_window_value(AT_DAIR, 4, AG_WINDOW_ANIM_FRAME_START, 8);




set_num_hitboxes(AT_DAIR,1);




set_hitbox_value(AT_DAIR, 1, HG_WINDOW, 2);
set_hitbox_value(AT_DAIR, 1, HG_LIFETIME, 6);
set_hitbox_value(AT_DAIR, 1, HG_HITBOX_TYPE, 1);

set_hitbox_value(AT_DAIR, 1, HG_SHAPE, 2);
set_hitbox_value(AT_DAIR, 1, HG_WIDTH, 46);
set_hitbox_value(AT_DAIR, 1, HG_HEIGHT, 10);
set_hitbox_value(AT_DAIR, 1, HG_HITBOX_X, 5);
set_hitbox_value(AT_DAIR, 1, HG_HITBOX_Y, -5);

set_hitbox_value(AT_DAIR, 1, HG_PRIORITY, 1);

set_hitbox_value(AT_DAIR, 1, HG_DAMAGE, 9);

set_hitbox_value(AT_DAIR, 1, HG_ANGLE, 270);

set_hitbox_value(AT_DAIR, 1, HG_BASE_KNOCKBACK, 8);
set_hitbox_value(AT_DAIR, 1, HG_KNOCKBACK_SCALING, 0.4);
//add hitstun?

set_hitbox_value(AT_DAIR, 1, HG_BASE_HITPAUSE, 8);
set_hitbox_value(AT_DAIR, 1, HG_HITPAUSE_SCALING, 0.4);
set_hitbox_value(AT_DAIR, 1, HG_HITBOX_GROUP, 1);

set_hitbox_value(AT_DAIR, 1, HG_VISUAL_EFFECT, 301);
set_hitbox_value(AT_DAIR, 1, HG_HIT_SFX, sound_get("dattack_hit"));