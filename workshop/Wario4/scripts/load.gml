sprite_change_offset("idle", 35, 70);
sprite_change_offset("hurt", 35, 70);
sprite_change_offset("crouch", 35, 68);
sprite_change_offset("walk", 37, 70);
sprite_change_offset("walkturn", 35, 68);
sprite_change_offset("dash", 37, 70);
sprite_change_offset("dashstart", 30, 68);
sprite_change_offset("dashstop", 30, 68);
sprite_change_offset("dashturn", 35, 68);

sprite_change_offset("jumpstart", 35, 68);
sprite_change_offset("jump", 35, 70);
sprite_change_offset("doublejump", 32, 64);
sprite_change_offset("walljump", 32, 64);
sprite_change_offset("pratfall", 35, 70);
sprite_change_offset("land", 35, 68);
sprite_change_offset("landinglag", 35, 68);

sprite_change_offset("parry", 35, 70);
sprite_change_offset("roll_forward", 35, 70);
sprite_change_offset("roll_backward", 32, 70);
sprite_change_offset("airdodge", 35, 70);
sprite_change_offset("waveland", 35, 70);
sprite_change_offset("tech", 35, 70);

sprite_change_offset("jab", 55 , 80);
sprite_change_offset("dattack", 50, 90);
sprite_change_offset("dattackair", 50, 90);
sprite_change_offset("dattackhit", 50, 90);
sprite_change_offset("ftilt", 60, 70);
sprite_change_offset("dtilt", 45, 50);
sprite_change_offset("utilt", 45, 80);
sprite_change_offset("nair", 60, 100);
sprite_change_offset("fair", 50, 90);
sprite_change_offset("fairair", 50, 90);
sprite_change_offset("bair", 64, 94);
sprite_change_offset("uair", 50, 90);
sprite_change_offset("dair", 35, 80);
sprite_change_offset("fstrong", 100, 150);
sprite_change_offset("ustrong", 80, 80);
sprite_change_offset("dstrong", 35, 70);
sprite_change_offset("nspecial", 70, 120);
sprite_change_offset("nspecialup", 70, 120);
sprite_change_offset("fspecial", 50, 100);
sprite_change_offset("uspecial", 45, 86);
sprite_change_offset("dspecial", 40, 70);
sprite_change_offset("taunt", 32, 62);

sprite_change_offset("plat", 48, 0);

sprite_change_offset("dspecial_proj", 40, 80)
sprite_change_offset("nspecial_proj", 64, 94);