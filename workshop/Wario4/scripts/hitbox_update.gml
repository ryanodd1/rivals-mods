// From the hitbox's perspective

if (attack == AT_DSPECIAL){
    hsp = ease_sineInOut(-5*direction, 5*direction, hitbox_timer + 7, 40);
}

if (attack == AT_NSPECIAL){
    if (hitbox_timer == 1){
        if (charged){
            if (aiming_up){
                grav = 0.6;
                hsp = 4 * direction;
                vsp = -16;
                kb_value = 4;
                kb_scale = 0.2;
                kb_angle = 361;
            }
            else{
                grav = 0;
                hsp = 14 * direction;
                vsp = 0;
                kb_value = 4;
                kb_scale = 0.2;
                kb_angle = 361;
            }
        }
        else{
            grav = 0.4;
            hsp = 4 * direction;
            vsp = 0;
            kb_value = 4;
            kb_scale = 0.2;
            kb_angle = 361;
        }
    }
    if (!free){
        vsp = -50;
        destroyed = true;
    }
}